# JSDoc Guideline

#### Constructor
```js
/**
	 * Limiter constructor
	 *
	 * @param {object} options - options parameters
	 * @param {object} options.redis_client - redis connection client instance
	 * @param {string} [options.key = limit:] - key of the limiter
	 * @param {number} [options.limit = 10] - rate limit within duration
	 * @param {number} [options.duration = 60]- within this period (second), number of 'limit' tokens can be used
	 * @constructor
	 */
	function Limiter(options) {
		this.redis_client = options.redis_client;
		this.key = options.key ? 'limit:' + options.key : 'limit:';
		this.limit = options.limit || 10;
		this.duration = options.duration || 60;

		this._lua_string = fs.readFileSync(__dirname + '/lua/get_token.lua').toString();
		this._lua_sha1 = null;
	}
```


#### Custom Type
```js
/**
 * Bigquery Streamer Config
 * @typedef {object} StreamerConfig
 *
 * redis
 * @property {object} [redis_client] - an redis_client to fetch data, if this is provided, redis_port and redis_host would be ignored.
 * @property {string} [redis_host] - address of the redis host to fetch data.
 * @property {number} [redis_port] - port of the redis host to fetch data.
 * @property {number} [redis_db] -  the db of redis to where streamer fetch data from.
 * @property {string} redis_namespace - namespace of keys to query
 *
 * scheduler
 * @property {number} schedule_interval - the interval for scheduler to scan new keys
 *
 * sender
 * @property {number} [max_idle_time = 10000] - Max idle time in millisecond for a sender to wait before next fetch
 * @property {number} [min_idle_time = 0] - Min idle time in millisecond for a sender to wait before next fetch
 * @property {number} [send_batch_size = 100] - the batch size of every BigQuery stream insert
 * @property {number} max_row_size - record over this size will be trimmed and archived to GCS
 * @property {TableDefinition} table_definitions
 *
 * sender|retry
 * @property {number} [retry_timeout = 300000] - retry timeout ref: https://github.com/jut-io/bluebird-retry/
 * @property {number} [retry_interval = 250] - retry interval ref: https://github.com/jut-io/bluebird-retry/
 * @property {number} [retry_backoff = 2] - retry backoff ref: https://github.com/jut-io/bluebird-retry/
 * @property {number} [retry_max_tries = 10] - retry max tries ref: https://github.com/jut-io/bluebird-retry/
 *
 * google cloud
 * @property {string} email - Google api authentication email
 * @property {string} bucket - bucket in Google Cloud Storage for archive
 * @property {string} project_id - project_id of destination BigQuery
 * @property {string} dataset_namespace - the (namespace) prefix of auto created datasets
 * @property {string} key_file_pem - file path of google auth private key pem file
 */


/**
 * Config Table Definition
 * @typedef {object} TableDefinition
 * @property {string} subject - subject of table
 * @property {string} subject.version - version of subject above
 * @property {object[]} subject.version.fields - array of field definitions, ref: https://cloud.google.com/bigquery/loading-data-into-bigquery
 * @property {string[]} subject.version.strippable_fields - array of fields which will be stripped when the item size is greater than max_row_size
 * @property {number} subject.version.ttl - ttl of table in days
 */


/**
 * Create a new Streamer
 * @constructor
 * @param {StreamerConfig} config
 * @constructor
 */
function BigQueryStreamer(config) {

	var self = this;

	this.params = {};
	this.params.config = {};
	this.params.service = {};

	//set up config
	this.applyConfig(config);

	//set up service

	//redis client
	var redis_client = config.redis_client || redis.createClient(config.redis_port, config.redis_host);
	if (config.redis_db) {
		redis_client.select(config.redis_db);
	}
	redis_client.on('error', function (err) {
		debug('streamer:redis').error(err)
	});
	this.params.service.promisified_redis_client = Promise.promisifyAll(redis_client);


	//big query client
	bigquery.tables = Promise.promisifyAll(bigquery.tables);
	bigquery.tabledata = Promise.promisifyAll(bigquery.tabledata);
	bigquery.datasets = Promise.promisifyAll(bigquery.datasets);
	this.params.service.promisified_bigquery_client = bigquery;


	//gcs client
	gcs.objects = Promise.promisifyAll(gcs.objects);
	this.params.service.promisified_gcs_client = gcs;

	//google_oauth_client
	this.params.service.google_client = new google.auth.JWT(
		this.params.config.email,
		this.params.config.key_file_pem,
		'key',
		[
			'https://www.googleapis.com/auth/bigquery',
			'https://www.googleapis.com/auth/devstorage.full_control'
		],
		''
	);
}
```